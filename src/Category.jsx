import React, {useState, useEffect} from 'react';
import axios from 'axios';
import CategoryCard from './CategoryCard';

const Category = () => {

    const [loading, setLoading] = useState(true);
    const [categories, setCategories] = useState([])
    const [columns, setColumns] = useState([])
    const [key, setKey] = useState('All')
    

useEffect(()=>{
    const api = async () => {
        await axios.get('https://www.themealdb.com/api/json/v1/1/categories.php')
        .then((response)=>{
            let data = response.data.categories
            setColumns(data)
            setLoading(false)
            data = Object.values(data)
            console.log(data.length)
            if( key !== 'All' ){
                data = data.filter((value) => {
                    if(value.idCategory == key){
                        return value
                    }else{
                        return ''
                    }
                 })
            }
            console.log(data)
            setCategories(data)
        })
    }
    if(loading){
        api()
    }
    })

    return (

        <div>
            <a className='text-xl mb-5 block' href="www.themealdb.com/api/json/v1/1/categories.php">Api - www.themealdb.com/api/json/v1/1/categories.php</a>
               <select onChange={(e) => {setKey(e.target.value); setLoading(true)}}
       className="md:w-80 md:mt-0 w-full mt-5 custom-select shadow-md rounded-md p-2 focus:outline-0 mb-4 mt-2 h-11"
       aria-label="Filter Countries By Region">
        <option value="All">Filter By Category</option>
       {columns && columns.map((category,key) => (
           <option value={category.idCategory} key={category.idCategory}>{category.strCategory}</option>
       ))}

        </select>

      <div className='grid grid-cols-1 xs:grid-cols-2 gap-2 lg:gap-4'>
            {categories && categories.map((category,key)=> (
            <div key={category.idCategory}>
            <CategoryCard category = {category}></CategoryCard>
            </div>
        ))}
      </div>
    
        </div>

       
    )
}

export default Category
