import React, {  useState, useEffect } from "react";
import styled from 'styled-components'
import axios from "axios";
import { useTable, useSortBy, usePagination } from 'react-table';
import './style.css'
import Category from "./Category";
import SearchIcon from "./SearchIcon";

const Styles = styled.div`
  max-width:100%;
  overflow-x:auto;

  table {
    border-spacing: 0;
    border: 1px solid #e2e8f0;
    margin: auto;
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }
thead{
  background-color:#4fd1c5;
  color:white;
}
th{
  border:0;
}
    th,
    td {
      margin: 0;
      padding: 0.5rem;
      // border-bottom: 1px solid black;
      border-right: 1px solid black;
      border-color:#e2e8f0;

      :last-child {
        border-right: 0;
      }
    }
    th{
      border-right:0;
    }
  }

  img{
    margin:auto;
    border-radius: 20px;
  }
  .pagination {
    padding: 0.5rem 0;
  }
`

function Table({ columns, data }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: {
        sortBy: [
            {
                id: 'idMeal',
                desc: false
            }
        ],
        pageSize : 5
    }
    },
    useSortBy,
    usePagination
  )

  // We don't want to render all 2000 rows for this example, so cap
  // it at 20 for this use case

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                // Add the sorting props to control sorting. For this example
                // we can add them into the header props
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
      <br />
      <div className="pagination">
        <button className="p-2 mx-1.5 bg-white shadow-md text-md lg:text-lg rounded-md focus:border-0" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button className="p-2 mx-1.5 bg-white shadow-md text-md lg:text-lg rounded-md focus:border-0" onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button className="p-2 mx-1.5 bg-white shadow-md text-md lg:text-lg rounded-md focus:border-0" onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button className="p-2 mx-1.5 bg-white shadow-md text-md lg:text-lg rounded-md focus:border-0" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {'>>'}
        </button>{' '} 
        <span>
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <br className="md:hidden"/>
        <select className="p-2 mt-3 mb-4 md:m-0 mx-1.5 bg-white shadow-md text-md lg:text-lg rounded-md focus:outline-0"
          value={pageSize}
          onChange={e => {
            setPageSize(Number(e.target.value))
          }}
        >
          {[5, 10, 20, 30].map(pageSize => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
    </>
  )
}

function App() {

  const [loadingData, setLoadingData] = useState(true);

  const columns = React.useMemo(
    () => [
      {
        Header: 'Meal ID',
        accessor: 'idMeal'
      },
      {
        Header: 'Meal Name',
        accessor: 'strMeal'
      },
      {
        Header: 'Category',
        accessor: 'strCategory'
      },
      {
        Header: 'Area',
        accessor: 'strArea'
      },
      {
        Header: 'Meal Image',
        accessor: 'strMealThumb',
        Cell: tableProps => (
          <img
            src={tableProps.row.original.strMealThumb}
            width={80}
          />
        )
      },
    ],
    []
  )

  const [data, setData] = useState([]);
  let url = 'https://www.themealdb.com/api/json/v1/1/search.php?s=';
  const [name, setName] = useState('');
  const search = (name) => {
    setName(name)
    setLoadingData(true)
  }

  useEffect(() => {
    const getData = async () => {
      await axios.get(url + name)
      .then((response)=>{
          setData(response.data.meals)
          setLoadingData(false)
      })
      
    }
    
    if (loadingData) {
      // if the result is not ready so you make the axios call
      getData();
    }
  })


  return (
    <div className="App font-sans">
      <div className="grid grid-cols-1 lg:grid-cols-2 divide-y lg:divide-y-0 lg:divide-x ">
       <div>
         <a className="text-xl mb-5 block" href="www.themealdb.com/api/json/v1/1/search.php?s=">Api - www.themealdb.com/api/json/v1/1/search.php?s=</a>

       <label><h2 className="text-xl mb-2">Search Meal by Name</h2></label>
      <div className="flex md:w-80 rounded-md pl-2 m-auto shadow-md">
      <input className="h-11 rounded-md focus:outline-0 mr-2 w-full" placeholder="Type to Search..." type="search" onChange={e => search(e.target.value)}/> 
      <button className="mx-2" onClick={()=>{search()}}><SearchIcon></SearchIcon></button>
      </div>
       {/* <input className="p-2 pl-5 pr-5 bg-gray-500 text-gray-100 text-lg rounded-lg focus:border-4 border-gray-300 focus:border-0" type="button" value="Search" onClick={()=>{search()}}/> */}
        <br />
       {data ? <Styles><Table columns={columns} data={data} />
       </Styles> : <h1 className="text-2xl"> <br/> No records found</h1>}
       </div> 
       <div>
         <Category></Category>
       </div>
      </div>
    </div>
  );
}

export default App;
