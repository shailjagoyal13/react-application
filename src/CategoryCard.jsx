import React, {useState} from 'react'

const CategoryCard = ({category}) => {
    const [showText, setShowText] = useState(false)
    const [btnClass, setBtnClass] = useState(true)

    let classes = 'description-hidden'
    let readMoreBtn = 'show-btn'
    let readLessBtn = 'hide-btn'

    if(showText){
        classes+= ' show'
    }
    if(btnClass){
        readMoreBtn = 'show-btn'
        readLessBtn = 'hide-btn'
    }
    else{
        readMoreBtn = 'hide-btn'
        readLessBtn = 'show-btn'
    }

    const readMore = () => {
        setShowText(true)
        setBtnClass(false)
    }
    const readLess = () => {
        setShowText(false)
        setBtnClass(true)

    }
    return (
        <div className='shadow md:m-2'>
            <img className='m-auto w-1/2 pt-4' src={category.strCategoryThumb} alt="" />
            <div className='p-2 bg-white'>
                <h3 className='text-lg font-medium'>{category.strCategory}</h3>
                <span className={classes}>{category.strCategoryDescription}
                </span>
                <button className={readMoreBtn} onClick={()=> readMore()}>Read More..</button>
                <button className={readLessBtn} onClick={()=> readLess()}>Hide..</button>
            </div>
        </div>
    )
}

export default CategoryCard
